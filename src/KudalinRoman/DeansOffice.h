#pragma once
#define RANDOM_MARKS_NUMBER 10
#include "Group.h"
#include "Student.h"
#include <string>
#include <fstream>
#include <ctime>
using namespace std;

class DeansOffice
{
private:
	Group **groups;
	Student **stdnt;
	int groupsNumber;
	int studentsNumber;
public:
	DeansOffice(): groups(nullptr), stdnt(nullptr), groupsNumber(0), studentsNumber(0) {};
	~DeansOffice()
	{
		for (int i = 0; i < groupsNumber; i++)
			delete groups[i];
		delete[] groups;
		groups = nullptr;
		for (int i = 0; i < studentsNumber; i++)
			delete stdnt[i];
		delete[] stdnt;
		stdnt = nullptr;
	}

	// ���������� ������ � ����� ������ �����.
	void addGroup(Group *gr)
	{
		if (!groupsNumber)
		{
			groups = new Group*[1];
			groups[groupsNumber] = gr;
		}
		else
		{
			Group **temp = new Group*[groupsNumber + 1];
			for (int i = 0; i < groupsNumber; i++)
				temp[i] = groups[i];
			delete[] groups;
			groups = temp;
			groups[groupsNumber] = gr;
		}
		groupsNumber++;
	}

	// ���������� �������� � ����� ������ ���������.
	void addStudent(Student *s)
	{
		if (!studentsNumber)
		{
			stdnt = new Student*[1];
			stdnt[studentsNumber] = s;
		}
		else
		{
			Student **temp = new Student*[studentsNumber + 1];
			for (int i = 0; i < studentsNumber; i++)
				temp[i] = stdnt[i];
			delete[] stdnt;
			stdnt = temp;
			stdnt[studentsNumber] = s;
		}
		studentsNumber++;
	}

	// �������� ������ ����� �� �����.
	void loadGroups()
	{
		ifstream groupsFile("..\\resources\\groups.txt");
		if (!groupsFile.is_open())
		{
			cout << "File \"groups.txt\" can not be opened" << endl;
			exit(EXIT_FAILURE);
		}
		while (!groupsFile.eof())
		{
			string currentGroup = "";
			getline(groupsFile, currentGroup);
			addGroup(new Group(currentGroup));
		}
		groupsFile.close();
	}

	// �������� ������ ��������� �� �����.
	void loadStudents()
	{
		ifstream studentsFile("..\\resources\\students.txt");
		if (!studentsFile.is_open())
		{
			cout << "File \"students.txt\" can not be opened" << endl;
			exit(EXIT_FAILURE);
		}
		while (!studentsFile.eof())
		{
			string currentID = "";
			string currentName = "";
			getline(studentsFile, currentID);
			getline(studentsFile, currentName);
			addStudent(new Student(atoi(currentID.c_str()), currentName));
		}
		studentsFile.close();
	}

	// ����� �������� ����� ������ ���� ���������.
	Student* findStudent(int id)
	{
		int i = 0;
		if (!studentsNumber)
		{
			cout << "There are no students in the University" << endl;
			return nullptr;
		}
		else
		{
			for (i = 0; i < studentsNumber; i++)
				if (stdnt[i]->getID() == id)
					return stdnt[i];
			if (i == studentsNumber)
			{
				cout << "There is no student with this ID in the University" << endl;
				return nullptr;
			}
		}
	}

	// ����� ������ ����� ������ ���� �����.
	Group* findGroup(string name)
	{
		int i = 0;
		if (!groupsNumber)
		{
			cout << "There are no groups in the University" << endl;
			return nullptr;
		}
		else
		{
			for (i = 0; i < groupsNumber; i++)
				if (strcmp(groups[i]->getTitle().c_str(), name.c_str()) == 0)
					return groups[i];
			if (i == groupsNumber)
			{
				cout << "There is no group with this title in the University" << endl;
				return nullptr;
			}
		}
	}

	
	// ��������� ������������� ��������� �� �������.
	void distributeStudents()
	{
		int curGr;	// �������� ��������������� ������ ������� ���������� �� ������.
		int maxGroupIndex = groupsNumber - 1;
		srand(time(0));
		for (int i = 0; i < studentsNumber; i++)
			if (!stdnt[i]->getGroup())
			{
				curGr = rand() % (maxGroupIndex + 1);
				groups[curGr]->addStudent(stdnt[i]);
			}
	}

	// ����������� ������ ��������� ������ ������� ��������.
	void randomMarks()
	{
		for (int i = 0; i < studentsNumber; i++)
			for (int j = 0; j < RANDOM_MARKS_NUMBER; j++)
				stdnt[i]->addMark(rand() % 5 + 1);
	}

	// ������� �������� � ������ ������.
	void replaceStudent(Student *st, Group *destination)
	{
		if (!st || !destination)
		{	cout << "Incorrect data! Try again" << endl;
			return;
		}
		if (!(st->getGroup()))
			return;
		st->getGroup()->removeStudent(st->getID());
		destination->addStudent(st);
	}

	// ������� ������ ���� �����. ����� ���������� � ������ � ���������� ������� ������.
	void groupsAvgMarks()
	{
		double maxAvgMark = 0.;
		Group *tmp = nullptr;
		cout << "\n**************************************\nGroup average mark:\n" << endl;
		for (int i = 0; i < groupsNumber; i++)
		{
			cout << groups[i]->getTitle() << ": ";
			cout << groups[i]->getAvgMark() << endl;
			if (groups[i]->getAvgMark() > maxAvgMark)
			{
				maxAvgMark = groups[i]->getAvgMark();
				tmp = groups[i];
			}
		}
		cout << "\nBest group:\n" << endl;
		cout << tmp->getTitle() << endl << "Average Mark: ";
		cout << tmp->getAvgMark() << endl << "\n**************************************\n" << endl;

	}

	// ������� ������ ���� ���������. ����� ���������� � �������� � ���������� ������� ������.
	void StudentsAvgMarks()
	{
		double maxAvgMark = 0.;
		Student *tmp = nullptr;
		cout << "\n**************************************\nStudent average mark:\n" << endl;
		for (int i = 0; i < studentsNumber; i++)
		{
			cout << stdnt[i]->getID() << " " << stdnt[i]->getFIO() << ": "  << stdnt[i]->getAvgMark() << endl;
			if (stdnt[i]->getAvgMark() > maxAvgMark)
			{
				maxAvgMark = stdnt[i]->getAvgMark();
				tmp = stdnt[i];
			}
		}
		cout << "\nBest student:\n" << endl;
		cout << tmp->getFIO() << endl << "Average Mark: ";
		cout << tmp->getAvgMark() << endl << "\n**************************************\n" << endl;

	}

	// �������� ���������� � ���� �������.
	void showGroups()
	{
		if (!groupsNumber)
		{
			cout << "There are no groups in the University" << endl;
			return;
		}
		else
		{
			cout << "\nUniversity Groups:" << endl;
			for (int i = 0; i < groupsNumber; i++)
				groups[i]->showGroupInfo(cout);
		}
	}

	// ������� ������ ���������, ���������� ID � ��� ������� ��������.
	void showStudents()
	{
		if (!studentsNumber)
		{	cout << "There are no students in the University" << endl;
			return;
		}
		else
		{
			cout << "\nUNIVERSITY STUDENTS:" << endl;
			for (int i = 0; i < studentsNumber; i++)
				cout << stdnt[i]->getID() << " " << stdnt[i]->getFIO() << endl;
		}
	}

	// �������� ���� ���������� ���� �����.
	void showHonours()
	{
		for (int i = 0; i < groupsNumber; i++)
			groups[i]->findHonoursPupils();
	}

	// ��������� ����� ������� ���� ����.
	void randomGroupsHeads()
	{
		for (int i = 0; i < groupsNumber; i++)
			groups[i]->randomHead();
	}

	// ���������� ���� ��������� �� ������� ������ ���� 3-��.
	void sendDown()
	{
		int curID = 0;	// ID �������� ��������.
		Student **tmp = nullptr;
		int count = studentsNumber;		// ����� ��������� ����� ���������� ���������.
		for (int i = 0; i < studentsNumber; i++)
			if (stdnt[i]->getAvgMark() < 3.0)
			{
				curID = stdnt[i]->getID();
				stdnt[i]->getGroup()->removeStudent(curID);
				delete stdnt[i];
				stdnt[i] = nullptr;
				count--;
			}

		// ��������� ��������� ������, ��������� �� ���������� �� ������������� 
		// ���������. 
		tmp = new Student*[count];
		count = 0;
		for (int i = 0; i < studentsNumber; i++)
			if (stdnt[i])
			{
				tmp[count] = stdnt[i];
				count++;
			}
		delete[] stdnt;
		stdnt = tmp;
		studentsNumber = count;
	}


	// ������ � ���� ������ ��������� ������������.
	void saveNewStudentsList()
	{
		ofstream stFile("..\\resources\\new_students.txt");
		if (!stFile.is_open())
		{
			cout << "Error while creating file" << endl;
			exit(EXIT_FAILURE);
		}
		stFile << "UNIVERSITY STUDENTS:\n" << endl;
		for (int i = 0; i < studentsNumber; i++)
			stFile << stdnt[i]->getID() << " " << stdnt[i]->getFIO() << endl;
		stFile.close();
	}

	// ������ � ���� ������ ����� ������������.
	void saveNewGroupsList()
	{
		ofstream grFile("..\\resources\\new_groups.txt");
		if (!grFile.is_open())
		{
			cout << "Error while creating file" << endl;
			exit(EXIT_FAILURE);
		}
		grFile << "UNIVERSITY GROUPS:\n" << endl;
		for (int i = 0 ; i < groupsNumber; i++)
			groups[i]->showGroupInfo(grFile);
		grFile.close();
	}
};